package in.gl.tddexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/products/{name}")
    public Product getProduct(@PathVariable String name){

        productService.getCloth();
        return productService.getByName(name);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void productNPE(ProductNotFoundException exception){

    }
}
