package in.gl.tddexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {


    private ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product getByName(String name) {
        Product product = productRepository.findByName(name);
        if(product == null)
            throw new ProductNotFoundException();

        getCloth();
        return product;
    }

    public String getCloth(){
        return "Ye Lo";
    }


    private String hello(){
        return null;
    }
}
