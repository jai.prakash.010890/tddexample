package in.gl.tddexample;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "product")
public class Product {
    @Id
    @Column(name="productid")
    private Long id;

    @Column(name="productname")
    private String name;
    @Column(name="color")
    private String colour;

    private int quantity;

    public Product(String name, String colour) {
        this.name = name;
        this.colour = colour;
    }

    public int getQuant(){
//        return this.quantity;
        return (this.quantity == 0 ? 1 : 0);
    }
}
