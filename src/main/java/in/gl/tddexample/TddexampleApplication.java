package in.gl.tddexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TddexampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(TddexampleApplication.class, args);
	}

}

/**
 *
 * controller
 *  return a product object
 *
 *
 */