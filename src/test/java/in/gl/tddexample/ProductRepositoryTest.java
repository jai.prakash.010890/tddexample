//package in.gl.tddexample;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//@RunWith(SpringRunner.class)
//@DataJpaTest
//public class ProductRepositoryTest {
//
//    @Autowired
//    private ProductRepository productRepository;
//
//    @Test
//    public void should_not_haveany_value() {
//
//        Iterable<Product> products = productRepository.findAll();
//        assertThat(products).isEmpty();
//    }
//
//
//    @Test
//    public void should_have_value() {
//        Product product = new Product(new Long(1),"T-shirt","red",6);
//        productRepository.save(product);
//
//       Product product1= productRepository.findByName("T-shirt");
//       assertThat(product1.getColour()).isEqualTo("red");
//    }
//
//
//}
