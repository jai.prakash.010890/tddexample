package in.gl.tddexample;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {


    @Mock
    private ProductRepository productRepository;


    private ProductService productService;

    @Before
    public void setup(){
        productService = new ProductService(productRepository);
    }


    @Test
    public void getProductShouldReturnProduct(){
        given(productRepository.findByName("T-shirt")).willReturn(new Product("T-shirt", "red"));

        Product product = productService.getByName("T-shirt");

        assertThat(product.getName()).isEqualTo("T-shirt");
        assertThat(product.getColour()).isEqualTo("red");


    }

    @Test(expected = ProductNotFoundException.class)
    public void productNPE(){
        given(productRepository.findByName("T-shirt")).willReturn(null);

        productService.getByName("T-shirt");
    }

    @Test
    public void testGetCloth(){

        String result = productService.getCloth();
        assertThat(result).isEqualTo("Ye Lo");
    }



}
