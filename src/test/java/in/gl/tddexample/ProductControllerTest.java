package in.gl.tddexample;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.BDDMockito.given;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @Test
    public void getProductShouldReturnProduct() throws Exception {

        given(productService.getByName(anyString())).willReturn(new Product("T-shirt", "red"));



        mockMvc.perform(MockMvcRequestBuilders.get("/products/T-shirt"))

                .andExpect(status().isOk())
                .andExpect(jsonPath("name").value("T-shirt"))
                .andExpect(jsonPath("colour").value("red"));

        verify(productService, times(2)).getCloth();

    }

    @Test
    public void getProduct_notFound() throws Exception {

        given(productService.getByName(anyString())).willThrow(new ProductNotFoundException());

        mockMvc.perform(MockMvcRequestBuilders.get("/products/T-shirt"))
                .andExpect(status().isNotFound());


    }
}
